/******************************************************************************
 * Buzz.ino
 * 
 * 2015-11-12 -- created
 * 2015-11-12 -- last updated
 * 
 * Based on the example sketch written by Dead Bug Prototypes (2014-02-07)
 * https://github.com/DeadBugPrototypes/DLS20
 * 
 * Changelog:
 ******************************************************************************/


/*******************************************************************************
 * Name:        buzz
 * Returns:     None.
 * Parameters:  int, pin number
 * Features:    Sounds the piezo buzzer alarm
 ******************************************************************************/
void buzz(int pinNo) {
  for(int i = 0; i < 3; i++) {
    delay(1000);
    for(int j = 0; j < 3; j++) {
      tone(pinNo, 440, 500);
      delay(1000);
    }
  }
}

