/******************************************************************************
 * DLS_Prototype_MPS.ino
 * 
 * 2015-10-07 -- created
 * 2015-11-12 -- last updated
 * 
 * Based on the example sketch written by Dead Bug Prototypes (2014-02-07)
 * https://github.com/DeadBugPrototypes/DLS20
 *
 * This script implements the SDISerial class written by J. Beasley (2014), 
 * https://github.com/joranbeasley/SDISerial, for communicating with 
 * environmental sensors by Decagon Devices, Inc.
 * 
 * Changelog:
 *  - defined sampling rate & response time [15.10.08]
 *  - created getMPS function [15.11.03]
 *  - added additional sampling rates [15.11.10]
 *  - removed while loop in setup [15.11.12]
 *  - added buzz function [15.11.12]
 ******************************************************************************/


/******************************************************************************
 * INCLUDE HEADERS
 ******************************************************************************/
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include "RTClib.h"
#include "SDISerial.h"


/******************************************************************************
 * SENSOR CONFIGURATION 
 *   BATT_PIN ........... analog pin for measuring 12 V battery
 *   BUZZ_PIN ........... digital pin supporting PWM for piezo buzzer
 *   DATA_PIN ........... digital pin that supports interrupts for MPS-6 
 *                        sensor connection
 *   LED_PIN ............ digital pin for LED notification of SD card writes
 *   VALVE_PIN .......... digital pin for valve actuation
 *   INVERTED ........... ??
 *   RESPONSE_TIME ...... how long to wait for a sensor response string; 
 *                        typically 1 second
 *   TIME_BETWEEN_MEAS .. time between two consecutvie sensor measurements 
 *                        during the same sampling period (milliseconds)
 *   VREF ............... reference voltage for analog reads
 ******************************************************************************/
#define BATT_PIN (A0)
#define BUZZ_PIN (6)
#define DATA_PIN (2)
#define LED_PIN (22)
#define VALVE_PIN (37)
#define INVERTED (1)
#define RESPONSE_TIME (1000)
#define TIME_BETWEEN_MEAS (100)
#define VREF (1100)


/******************************************************************************
 * DATALOGGING CONFIGURATION
 *   DS1337_ADDRESS ..... real-time clock address
 *   SD_CHIPSELECT ...... SD card chip select
 *   NORMAL_RATE ........ RTC alarm time between sensor sampling and saves
 ******************************************************************************/
#define DS1337_ADDRESS (0x68)
#define SD_CHIPSELECT (8)
#define NORMAL_RATE (10)


/******************************************************************************
 * INTERNAL STATE VARIABLES 
 *   RTC .............. DS1307 real-time clock object
 *   sdi .............. SDISerial object
 *   myFile ........... File object (for SD card)
 *   numberOfSensors .. number of analog sensors
 *   sensorPorts ...... char array for sensor port IDs
 *                       - green: 0
 *                       - orange: 1
 *                       - yellow: 2
 *                       - red: 3
 *                       - blue: 4
 *   dataArray ........ char array for MPS-6 measurements
 *   my_pos ........... char array position for writing
 *   MyTimeStamp ...... char array for timestamping
 ******************************************************************************/
RTC_DS1307 RTC;
SDISerial sdi(DATA_PIN, INVERTED);
File myFile;
const int numberOfSensors = 1;
char sensorPorts[] = {'1'};
char dataArray[18*numberOfSensors + 1];
int my_pos = 0;
char MyTimeStamp[] = "0000-00-00,00:00:00";


/******************************************************************************
 *  Name:        setup
 *  Returns:     None.
 *  Parameters:  None.
 *  Features:    Sets digital pins for output; 
 *               Sets analog reference voltage to 1.1 V;
 *               Samples and saves data measurements to SD card
 ******************************************************************************/
void setup(){
  pinMode(10, OUTPUT);  // SD card digital pin setup
  pinMode(53, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUZZ_PIN, OUTPUT);
  pinMode(VALVE_PIN, OUTPUT);

  // Set analog reference voltage (Arduino Mega only)
  analogReference(INTERNAL1V1);
  
  // start SDI connection and UART
  sdi.begin();
  Serial.begin(9600);
  delay(3000);

  GetDate();
  for (byte k = 0; k < numberOfSensors; k++) {
    getMPS(k);
    delay(TIME_BETWEEN_MEAS);
  }
  dataArray[my_pos] = '\0';
  WriteSD();
}


/******************************************************************************
 *  Name:        loop
 *  Returns:     None.
 *  Parameters:  None.
 *  Features:    Checks 12 V battery status and sound alarm if too low;
 *               Checks water potential and begins irrigation if too low;
 *               Otherwise, sets the RTC alarm for next sampling
 ******************************************************************************/
void loop() {
  unsigned int my_battery;
  float wp;
  unsigned int battery_threshold = 900;
  float end_threshold = -200.0;
  float start_threshold = -1000.0;

  // Check 12 V battery status & sound the alarm if too low:
  my_battery = getVolt();
  
  if (my_battery < battery_threshold) {
    digitalWrite(VALVE_PIN, LOW);
    while (true) {
      buzz(BUZZ_PIN);
    }
  } else {
    // Check the water status & start irrigation if too low:
    wp = sampleMPS(0);
    
    /*
    Serial.print("Water potential: ");
    Serial.println(wp);
    */
    
    if (wp > end_threshold) {      
      digitalWrite(VALVE_PIN, LOW);
      SetAlarmMin(NORMAL_RATE);
      delay(500);

      /*
      Serial.println("Stop irrigation!");
      */

    } else  if (wp < start_threshold) {
      digitalWrite(VALVE_PIN, HIGH);
      delay(500);

      /*
      Serial.println("Start irrigation!");
      */
      
    }
  }
}

