/******************************************************************************
 * MPS.ino
 * 
 * 2015-07-13 -- created
 * 2015-11-12 -- last updated
 * 
 * Based on the example sketch written by Dead Bug Prototypes (2014-02-07)
 * https://github.com/DeadBugPrototypes/DLS20
 * 
 * Changelog:
 *   - moved everything into a single function call [15.11.04]
 *   - removed integer rounding (saves to char array) [15.11.10]
 ******************************************************************************/


/******************************************************************************
 * Name:        getMPS
 * Returns:     None.
 * Parameters:  int, sensor port index
 * Features:    Requests measurement from MPS-6 sensor and saves to the 
 *              global dataArray
 ******************************************************************************/
void getMPS(int i){
  // Get sensor port ID:
  char id = sensorPorts[i];

  // Request a measurement:
  char command_a[] = {'0', 'M', '!', '\0'};
  command_a[0] = id;
  
  sdi.sdi_cmd(command_a);
  char* temp_array1 = sdi.wait_for_response(RESPONSE_TIME);
  
  delay(1000);

  // Get measurement:
  char command_b[] = {'0', 'D', '0', '!', '\0'};
  command_b[0] = id;
  
  sdi.sdi_cmd(command_b);
  char* temp_array2 = sdi.wait_for_response(RESPONSE_TIME);
  
  char dataReturn[18];
  size_t dr_size = sizeof(dataReturn);
  strncpy(dataReturn, temp_array2, dr_size);
  dataReturn[dr_size - 1] = '\0';

  /*
  Serial.print(i);
  Serial.print(": ");
  Serial.println(dataReturn);
  */
  
  // Parse the data into water pot. and temperature. 
  int pos = 0;
  byte array_length = strlen(dataReturn);

  /*
  Serial.print("Length: ");
  Serial.println(array_length);
  */
  
  // skip first array element (this is the sensor port number).
  for (byte j = 1; j < array_length - 1; j++) {
    char myChar = dataReturn[j];
    
    if (j != 1 && (myChar == '-' || myChar == '+')) {
      dataArray[my_pos] = ',';
      my_pos++;

      /*
      Serial.print("   ");
      Serial.print(my_pos-1);
      Serial.println(":,");
      */
    }
    
    dataArray[my_pos] = myChar;
    my_pos++;

    /*
    Serial.print("   ");
    Serial.print(my_pos-1);
    Serial.print(":");
    Serial.println(myChar);
    */
  }
  
  dataArray[my_pos] = ',';
  my_pos++;
  
  /*
  Serial.print("   ");
  Serial.print(my_pos-1);
  Serial.println(":,");
  */
}


/******************************************************************************
 * Name:        sampleMPS
 * Returns:     float, water potential, kPa
 * Parameters:  int, sensor port index
 * Features:    Requests measurement from MPS-6 sensor
 ******************************************************************************/
float sampleMPS(int i){
  // Get sensor port ID:
  char id = sensorPorts[i];

  // Request a measurement:
  char command_a[] = {'0', 'M', '!', '\0'};
  command_a[0] = id;
  
  sdi.sdi_cmd(command_a);
  char* temp_array1 = sdi.wait_for_response(RESPONSE_TIME);
  
  delay(1000);

  // Get measurement:
  char command_b[] = {'0', 'D', '0', '!', '\0'};
  command_b[0] = id;
  
  sdi.sdi_cmd(command_b);
  char* temp_array2 = sdi.wait_for_response(RESPONSE_TIME);
  
  char dataReturn[18];
  size_t dr_size = sizeof(dataReturn);
  strncpy(dataReturn, temp_array2, dr_size);
  dataReturn[dr_size - 1] = '\0';

  // Parse the data into water pot. and temperature. 
  int pos = 0;
  byte array_length = strlen(dataReturn);

  // skip first array element (this is the sensor port number).
  for (byte i = 1; i < array_length - 1; i++) {
    char myChar = dataReturn[i];
    if (i != 1 && (myChar == '-' || myChar == '+')) {
      pos = i;
    }
  }

  // Convert water pot. to float:
  char buffer_a[pos];
  for (byte i = 1; i < pos; i++) {
    buffer_a[i-1] = dataReturn[i];
  }
  buffer_a[pos-1] = '\0';
  
  float n = atof(buffer_a);
  return n;
}


