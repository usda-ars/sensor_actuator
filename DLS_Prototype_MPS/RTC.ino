/******************************************************************************
 * RTC.ino
 * 
 * 2015-07-10 -- created
 * 2015-11-12 -- last updated
 * 
 * Based on the example sketch written by Dead Bug Prototypes (2014-02-07)
 * https://github.com/DeadBugPrototypes/DLS20
 * 
 * Changelog:
 ******************************************************************************/


/******************************************************************************
 * Name:        GetDate
 * Returns:     None.
 * Parameters:  None.
 * Features:    Saves the current timestamp to MyTimeStamp global variable
 ******************************************************************************/
void GetDate()
{
  Wire.begin(DS1337_ADDRESS);
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 0);	
  Wire.endTransmission();

  Wire.requestFrom(DS1337_ADDRESS, 7);
  uint8_t ss = bcd2bin(Wire.read());
  uint8_t mm = bcd2bin(Wire.read());
  uint8_t hh = bcd2bin(Wire.read());
  Wire.read();
  uint8_t d = bcd2bin(Wire.read());
  uint8_t m = bcd2bin(Wire.read());
  uint16_t y = bcd2bin(Wire.read()) + 2000;

  sprintf(
    MyTimeStamp,
    "%04d-%02d-%02d %02d:%02d:%02d", 
    y, m, d, hh, mm, ss 
  );
}


/******************************************************************************
 * Name:        SetAlarmMin
 * Returns:     None.
 * Parameters:  int, number of minutes (AntalMin)
 * Features:    Sets the RTC alarm to trigger a specified number of minutes 
 *              in the future
 ******************************************************************************/
void SetAlarmMin(uint8_t AntallMin)
{
  Wire.begin(DS1337_ADDRESS);
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 0);	
  Wire.endTransmission();

  Wire.requestFrom(DS1337_ADDRESS, 2);
  Wire.read(); // hopper over sekunder
  uint8_t mm = bcd2bin(Wire.read());
  
  mm = mm + AntallMin;
  if (mm > 59) mm = mm - 60; 
  
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 7);
  Wire.write(0b00000000);      // Alarm 1 sekunder
  Wire.write(bin2bcd(mm));     // Alarm 1 minutter
  Wire.write(0b10000000);      // Alarm 1 timer (flagg satt)
  Wire.write(0b10000000);      // Alarm 1 dato (flagg satt)
  Wire.write(0b00000000);      // Alarm 2 minutter
  Wire.write(0b00000000);      // Alarm 2 timer (flagg satt)
  Wire.write(0b00000000);      // Alarm 2 dato (flagg satt)
  Wire.write(0b00011101);      // skru av ocillator og enable alarm 1
  Wire.write((byte) 0);        // Nullstill alt
  Wire.endTransmission();
}


/******************************************************************************
 * Name:        SetAlarm
 * Returns:     None.
 * Parameters:  None.
 * Features:    Sets a 30 second RTC alarm
 ******************************************************************************/
void SetAlarm()
{
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 7);
  Wire.write(bin2bcd(29));     // Alarm 1 sekunder
  Wire.write(0b10000000);      // Alarm 1 minutter
  Wire.write(0b10000000);      // Alarm 1 timer (flagg satt)
  Wire.write(0b10000000);      // Alarm 1 dato (flagg satt)
  Wire.write(0b10000000);      // Alarm 2 minutter
  Wire.write(0b10000000);      // Alarm 2 timer (flagg satt)
  Wire.write(0b10000000);      // Alarm 2 dato (flagg satt)
  Wire.write(0b00011111);      // skru av ocillator og enable alarm 1 & 2
  Wire.write((byte) 0);        // Nullstill alt
  Wire.endTransmission();
}


/******************************************************************************
 * Name:        bcd2bin
 * Returns:     binary
 * Parameters:  binary coded decimal (val)
 * Features:    Converts binary coded decimal to normal decimal
 ******************************************************************************/
static uint8_t bcd2bin (uint8_t val) { 
  return val - 6 * (val >> 4); 
}


/******************************************************************************
 * Name:        bin2bcd
 * Returns:     binary coded decimal
 * Parameters:  binary (val)
 * Features:    Converts normal decimal to binary coded decimal
 ******************************************************************************/
static uint8_t bin2bcd (uint8_t val) { 
  return val + 6 * (val / 10); 
}


