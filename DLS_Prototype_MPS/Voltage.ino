/******************************************************************************
 * Voltage.ino
 * 
 * 2014-02-07 -- created
 * 2015-11-12 -- last updated
 * 
 * Based on the example sketch written by Dead Bug Prototypes (2014-02-07)
 * https://github.com/DeadBugPrototypes/DLS20
 * 
 * Changelog:
 *   - removed scaling factor [15.11.12]
 *   - changed to read battery status from BATT_PIN
 ******************************************************************************/


/*******************************************************************************
 * Name:        getVolt
 * Returns:     u_int, supply voltage (mV)
 * Parameters:  None.
 * Features:    Returns supply voltage measured through a 10k/15k voltage 
 *              divider (R4 and R5 resistors); currently set to the analog 
 *              value because there's no way to back out actual input voltage.
 *              Vout = 0.6*Vin, where Vin = VREF*RAW/1023
 *              
 *              I'm not convinced that this does anything meaningful.
 ******************************************************************************/
int getVolt(){
  int vout_raw;          // 10-bit analog reading
  float temp;            // temporary variable
  unsigned int vin_mv;   // scaled supply voltage (mV)
  float sf = 11.2;       // scaling factor for voltage divider
  
  // Convert 10-bit analog to mV and scale to Vin
  vout_raw = analogRead(BATT_PIN);

  /*
  Serial.print("Raw analog read: ");
  Serial.println(vout_raw);
  */
  
  temp = (float)vout_raw;
  temp /= 1023.0;
  temp *= (float)VREF;
  temp *= sf;

  /*
  Serial.print("Voltage: ");
  Serial.println(temp);
  */
  
  vin_mv = (int)(temp + 0.5);
  return vin_mv;
}

