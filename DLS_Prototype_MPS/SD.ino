/******************************************************************************
 * SD.ino
 * 
 * 2014-02-07 -- created
 * 2015-11-12 -- last updated
 * 
 * Based on the example sketch written by Dead Bug Prototypes (2014-02-07)
 * https://github.com/DeadBugPrototypes/DLS20
 * 
 * Changelog:
 *   - renamed DATALOG.TXT to datalog.txt [15.07.10]
 *   - added file existence checking, creates new if not [15.07.10]
 *   - updated headerline & Serial print for numberOfSensors [15.08.13]
 *   - added LED indication of SD card writing [15.11.12]
 *   - wrapped LED light on/off with delays [15.11.12]
 ******************************************************************************/


/******************************************************************************
 * Name:        WriteSD
 * Returns:     None.
 * Parameters:  None.
 * Features:    Writes data to SD card using the SD library
 *              http://arduino.cc/en/Reference/SD
 *              Note: chip select is set to pin 8
 ******************************************************************************/
void WriteSD() {
  if (!SD.begin(SD_CHIPSELECT)) {
    Serial.print("Initialization failed! "); 
    Serial.println("No SD card or not correctly mounted?");
    return;
  }

  // Light LED to indicate SD card in use:
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  
  // Check if datafile exists, if not, create it with headerline:
  if (!SD.exists("datalog.txt")) {
    Serial.println("Creating data log file.");
    myFile = SD.open("datalog.txt", FILE_WRITE);

    /* DATA FILE HEADER LINE */
    myFile.print("Timestamp,");
    for (byte i=0; i<numberOfSensors; i++) {
      char id = sensorPorts[i];
      myFile.print("MP6-");
      myFile.print(id);
      myFile.print("_kPa,");
      myFile.print("MP6-");
      myFile.print(id);
      myFile.print("_degC,");
    }
    myFile.println("Battery_bit");
    myFile.close();
  }
  
  myFile = SD.open("datalog.txt", FILE_WRITE);
  if (myFile) {
    myFile.print(MyTimeStamp);
    myFile.print(",");
    myFile.print(dataArray);
    myFile.println( getVolt() );
    myFile.close();
  } else {
    Serial.println("Error writing to datalog.txt");
  }

  Serial.print(MyTimeStamp);
  Serial.print(",");
  Serial.print(dataArray);
  Serial.println( getVolt() );

  // Done writing, turn off LED:
  delay(500);
  digitalWrite(LED_PIN, LOW);
}


