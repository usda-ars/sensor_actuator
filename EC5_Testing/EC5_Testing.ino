/* test_ec5.ino
 *  
 *  written by Tyler W. Davis
 *  Robert W. Holley Center, USDA-ARS
 *  
 *  2015-08-11 -- created
 *  2015-08-13 -- last updated 
 *  
 *  This script tests the EC5 measurements on Arduino before
 *  moving to the DLS2.0.
 *  
 *  changelog:
 *  00. Sucessfully tested single EC-5 with digital pin controlled
 *      NPN transistor (3.3 V excitation)
 *  01. Updated for Arduino Mega
 *  
 */
#define EXCITE_TIME (10)
#define TIME_BETWEEN_MEAS (20)

/* GLOBAL VARIABLES */
const int NumberOfSensors = 4;
byte MyAnalogIns[] = {8, 9, 10, 11};
byte MyExcitePins[] = {30, 28, 26, 24};
int MySensorReads[NumberOfSensors+1];

void setup() {
  Serial.begin(9600);
  delay(1000);
  //analogReference(INTERNAL);    // for Arduino Uno
  analogReference(INTERNAL1V1); // for Arduino Mega

  for (byte i = 0; i < NumberOfSensors; i++) {
    pinMode(MyExcitePins[i], OUTPUT);
    digitalWrite(MyExcitePins[i], LOW);
  }
}

void loop() {
  /*  Turn sensor on, 
   *  wait for excitation time,
   *  read the analog measurement,
   *  turn sensor off,
   *  allow sensor to discharge
   */ 
  for (byte i = 0; i < NumberOfSensors; i++) {
    digitalWrite(MyExcitePins[i], HIGH);
    delay(EXCITE_TIME);
    MySensorReads[i] = analogRead(MyAnalogIns[i]);
    digitalWrite(MyExcitePins[i], LOW);
    delay(TIME_BETWEEN_MEAS);
  }
  
  Serial.print("Analog measurements: ");
  for (byte i = 0; i < NumberOfSensors-1; i++) {
    Serial.print(MySensorReads[i]);
    Serial.print(",");
  }
  Serial.println(MySensorReads[NumberOfSensors-1]);
  delay(3000);
}
