/* calibrate_ec5.ino
 *  
 *  written by Tyler W. Davis
 *  Robert W. Holley Center, USDA-ARS
 *  
 *  2015-08-11 -- created
 *  2015-08-25 -- last updated 
 *  
 *  ~~~~~~~~~~~~
 *  description:
 *  ~~~~~~~~~~~~
 *  This script is to be used for calibrating the EC-5 sensors.
 *  
 *  ~~~~~~~~~~
 *  changelog:
 *  ~~~~~~~~~~
 *  
 *  ~~~~~
 *  todo:
 *  ~~~~~
 *  - add write to SD card
 *  
 */

/* INCLUDE HEADERS */
#include <SPI.h>
#include <SD.h>

/**************************************************************
 * SENSOR CONFIGURATION 
 *   NUMSAMPLES ......... sensor measurements to take
 *   EXCITE_TIME ........ excitation time (milliseconds)
 *   TIME_BETWEEN_MEAS .. time between two consecutvie sensor 
 *                        measurements during the same sampling 
 *                        period (milliseconds)
 **************************************************************/
#define NUMSAMPLES (12)
#define EXCITE_TIME (10)
#define TIME_BETWEEN_MEAS (40)

/***************************************************
 * DATALOGGING CONFIGURATION
 *   SD_CHIPSELECT ...... SD card chip select
 *   ISLOGGER ........... set to 0 for serial print
 **************************************************/
#define SD_CHIPSELECT (8)
#define ISLOGGER (0)

/*****************************************************
 * INTERNAL STATE VARIABLES 
 *   myFile ........... File object (for SD card)
 *   numberOfSensors .. number of analog sensors
 *   analogPins ....... array of analog sensor input 
 *                      pins (match with excitePins)
 *   excitePins ....... array of digital pins for 
 *                      excitation corresponding to 
 *                      their analog sensor inputs
 *   dataArray ........ integer array for storing 
 *                      sensor measurements and 
 *                      battery status; note that 
 *                      array size must have length 
 *                      one greater than expected 
 *                      number of entries!
 *****************************************************/
File myFile;
const int numberOfSensors = 5;
byte analogPins[] = {8, 9, 10, 11, 12};
byte excitePins[] = {30, 28, 26, 24, 22};
int dataArray[numberOfSensors+1];

/***************************************************
 *  Name:        setup
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Make measurements and save to SD 
 *               card.
 *
 ***************************************************/
void setup() {
  Serial.begin(9600);
  delay(1000);
  //analogReference(INTERNAL);    // for Arduino Uno
  analogReference(INTERNAL1V1);  // for Arduino Mega

  // Set all digital pins for excitation to low:
  for (byte i = 0; i < numberOfSensors; i++) {
    pinMode(excitePins[i], OUTPUT);
    digitalWrite(excitePins[i], LOW);
  }

  /* @TODO
   *  move sensor sampling and writing to SD card here and make
   *  the loop do nothing; therefore, the reset button will be 
   *  used to trigger a calibration measurement
   */
  if (!SD.begin(SD_CHIPSELECT)) {
    Serial.print("Initialization failed! "); 
    Serial.println("No SD card or not correctly mounted?");
    return;
  }

  // Check if datafile exists, if not, create it with headerline:
  if (!SD.exists("datalog.txt")) {
    Serial.println("Creating data log file.");
    myFile = SD.open("datalog.txt", FILE_WRITE);

    /* DATA FILE HEADER LINE */
    for (byte i=0; i<numberOfSensors-1; i++) {
      myFile.print("EC5-");
      myFile.print(i+1);
      myFile.print("_bit,");
    }
    myFile.print("EC5-");
    myFile.print(numberOfSensors);
    myFile.println("_bit");
    myFile.close();
  }
   
  // Loop over each sample:
  for (byte j = 0; j < NUMSAMPLES; j++) {
    
    // Loop over each sensor:
    for (byte i = 0; i < numberOfSensors; i++) {
      
      // Sample analog measurements:
      digitalWrite(excitePins[i], HIGH);
      delay(EXCITE_TIME);
      dataArray[i] = analogRead(analogPins[i]);
      digitalWrite(excitePins[i], LOW);
      delay(TIME_BETWEEN_MEAS);
    }

    // Save/print measurements:
    WriteSD();
    delay(250);
    
  } // end sampling loop

  // Reset dataArray and save blank line to file:
  for (byte i = 0; i < numberOfSensors; i++) {
    dataArray[i] = 0;
  }
  WriteSD();
  delay(250);
  
}

/***************************************************
 *  Name:        loop
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Nothing.
 *
 ***************************************************/
void loop() {
  /*  Nothing to do here.
   */ 
}

/***************************************************
 *  Name:        WriteSD()
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Saves measurement to SD card.
 *
 ***************************************************/
void WriteSD()
{
  myFile = SD.open("datalog.txt", FILE_WRITE);
  if (myFile) {
    for (byte i=0; i<numberOfSensors-1; i++) {
      myFile.print(dataArray[i]);
      myFile.print(",");
    }
    myFile.println(dataArray[numberOfSensors-1]);
    myFile.close();
    
    if (!ISLOGGER) {
      Serial.print("Writing: ");
      for (byte i=0; i<numberOfSensors-1; i++) {
        Serial.print(dataArray[i]);
        Serial.print(",");
      }
      Serial.println(dataArray[numberOfSensors-1]);
    }
  } else {
    Serial.println("Error writing to datalog.txt");
  }
}
