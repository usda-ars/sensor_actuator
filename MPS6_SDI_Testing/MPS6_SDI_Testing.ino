/*******************************************************************************
 * SDISerialExample.ino
 * 
 * written by Tyler W. Davis
 * USDA-Agricultural Research Service
 * Robert W. Holley Center for Agriculture & Health
 * Ithaca, NY 14853
 * 
 * 2015-10-07 -- created
 * 2015-10-08 -- last updated
 * 
 * ~~~~~~~~~~~~
 * description:
 * ~~~~~~~~~~~~
 * This script implements the SDISerial class written by J. Beasley (2014), 
 * https://github.com/joranbeasley/SDISerial, for communicating with 
 * environmental sensors by Decagon Devices, Inc.
 * 
 * ~~~~~~~~~~
 * changelog:
 * ~~~~~~~~~~
 * 01. defined sampling rate & response time [15.10.08]
 * 
 ******************************************************************************/

/* INCLUDE HEADERS */
#include <SDISerial.h>


/**************************************************************
 * SENSOR CONFIGURATION 
 *   DATA_PIN ......... digital pin that supports interrupts
 *   INVERTED ......... ??
 *   RESPONSE_TIME .... how long to wait for a sensor response
 *                      string, typically 1 second
 *   TIME_BETWEEN_MEAS .. time between two consecutvie sensor 
 *                        measurements during the same sampling 
 *                        period (milliseconds)
 **************************************************************/
#define DATA_PIN (2)
#define INVERTED (1)
#define SAMPLING_RATE_MS (2000)
#define RESPONSE_TIME (1000)
#define TIME_BETWEEN_MEAS (200)


/***************************************************
 * DATALOGGING CONFIGURATION
 *   SAMPLING_RATE_MS .. sampling rate (ms)
 **************************************************/
#define SAMPLING_RATE_MS (2000)


/*****************************************************
 * INTERNAL STATE VARIABLES 
 *   k ................ flag for serial port changing
 *   sdi .............. SDISerial object
 *   numberOfSensors .. number of analog sensors
 *   sensorPorts ...... char array for sensor port IDs
 *   dataArray ........ integer array for storing 
 *                      sensor measurements and 
 *                      battery status; note that 
 *                      array size must have length 
 *                      one greater than expected 
 *                      number of entries!
 *   sensorInfo ....... char array for storing sensor 
 *                      information
 *   measRequest ...... char array for storing 
 *                      measurement request response
 *   dataReturn ....... char array for storing MPS-6
 *                      measurement data
 *****************************************************/
//byte k = 0;
SDISerial sdi(DATA_PIN, INVERTED);
const int numberOfSensors = 4;
char sensorPorts[] = {'0', '1', '2', '3'};
int dataArray[9];

char sensorInfo[34];
char measRequest[6];
char dataReturn[18];


/***************************************************
 *  Name:        setup
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Begin serial communications
 *
 ***************************************************/
void setup(){
  // start SDI connection and UART
  sdi.begin();
  Serial.begin(9600);
  while(!Serial) {
    ;
  }
  
  // allow sensors to powerup and output DDI serial string
  delay(3000);
}


/***************************************************
 *  Name:        loop
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Requests info and measurement from
 *               sensor and prints to console.
 *
 ***************************************************/
void loop(){
  /*
  // Change sensor's serial port ID:
  if (k == 0) {
    change_address('4', '0');
    k = k + 1;
  }
  */

  for (byte i = 0; i < numberOfSensors; i++){
    char id = sensorPorts[i];
    
    //get_sensor_info(id);
    request_measurement(id);
    delay(1000); // @TODO defined by measurement request
    get_measurement(id);
    
    Serial.print(id);
    Serial.print(": ");
    print_measurement();
    
    //Serial.println(sensorInfo);
    //Serial.println(measRequest);
    //Serial.println(dataReturn);
    delay(TIME_BETWEEN_MEAS);
  }
  
  delay(SAMPLING_RATE_MS);
}


/***************************************************
 *  Name:        get_sensor_info
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  char, serial port number (i)
 *
 *  Description: Updates sensor information to the 
 *               global char array.
 *
 ***************************************************/
void get_sensor_info(char i){
  // Define the basic info request command:
  char command[] = {'0', 'I', '!', '\0'};
  command[0] = i;
  
  sdi.sdi_cmd(command);
  char* temp_array = sdi.wait_for_response(RESPONSE_TIME);
  
  size_t destination_size = sizeof(sensorInfo);
  strncpy(sensorInfo, temp_array, destination_size);
  sensorInfo[destination_size - 1] = '\0';
}


/***************************************************
 *  Name:        request_measurement
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  char, sensor port number (i)
 *
 *  Description: Makes request to sensor for making 
 *               a measurement; updates the 
 *               measurement request global char
 *               array
 *
 ***************************************************/
 void request_measurement(char i){
  // Define the measurement request command:
  char command[] = {'0', 'M', '!', '\0'};
  command[0] = i;
  
  sdi.sdi_cmd(command);
  char* temp_array1 = sdi.wait_for_response(RESPONSE_TIME);
  
  size_t destination_size = sizeof(measRequest);
  strncpy(measRequest, temp_array1, destination_size);
  measRequest[destination_size - 1] = '\0';
}


/***************************************************
 *  Name:        get_measurement
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  char, sensor serial port (i)
 *
 *  Description: Updates global char array for 
 *               sensor measurement.
 *
 ***************************************************/
void get_measurement(char i){
  // Define data request command:
  char command[] = {'0', 'D', '0', '!', '\0'};
  command[0] = i;
  
  sdi.sdi_cmd(command);
  char* temp_array2 = sdi.wait_for_response(RESPONSE_TIME);
  
  size_t destination_size = sizeof(dataReturn);
  strncpy(dataReturn, temp_array2, destination_size);
  dataReturn[destination_size - 1] = '\0';
}


int parse_measurement() {
  // Find the position where the two integers are to be broken. 
  // Skip first array element (this is the sensor port number).
  int pos = 0;
  byte array_length = sizeof(dataReturn);
  
  for (byte i = 1; i < array_length - 1; i++) {
    char myChar = dataReturn[i];
    if (i != 1 && (myChar == '-' || myChar == '+')) {
      pos = i;
    }
  }
  //Serial.print("Found break character at position ");
  //Serial.println(pos);
  return pos;
}

void print_measurement() {
  // Get the parsing parameter:
  int pos = parse_measurement();
  for (byte i = 1; i < pos; i++) {
    Serial.print(dataReturn[i]);
  }
  Serial.print(",");
  for (byte i = pos; i < sizeof(dataReturn) - 1; i++) {
    Serial.print(dataReturn[i]);
  }
  Serial.println("");
}

/***************************************************
 *  Name:        change_address
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  - char, old sensor port number (i)
 *               - char, new sensor port number (j)
 *
 *  Description: Changes a sensor's port number.
 *
 ***************************************************/
void change_address(char from_i, char to_i) {
  // Define the request for port number change command:
  char command[] = {'1', 'A', '0', '!', '\0'};
  command[0] = from_i;
  command[2] = to_i;
  
  sdi.sdi_cmd(command);
  char* temp_array = sdi.wait_for_response(RESPONSE_TIME);

  // Print success or failure (for debugging):
  if (temp_array[0] == to_i) {
    Serial.print("Address successfully changed from ");
    Serial.print(from_i);
    Serial.print(" to ");
    Serial.println(to_i);
  } else {
    Serial.println("Failed to change address!");
  }
}
