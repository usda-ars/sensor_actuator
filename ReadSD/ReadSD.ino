/*
  SD card read
 
 This example shows how to read data from an SD card file 	

 The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 
 created   Nov 2010
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.

 2015-07-10 -- last updated

 changelog:
   - must include SPI.h library [15.07.10]
 
 */

#include <SPI.h>
#include <SD.h>

/* CS pin for SD card */
#define SD_CHIPSELECT (8)

File myFile;

void setup()
{
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }


  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
  pinMode(10, OUTPUT);
  pinMode(53, OUTPUT);
   
  if (!SD.begin(SD_CHIPSELECT)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  myFile = SD.open("DATALOG.TXT");
  if (myFile) {
    Serial.println("DATALOG.TXT content:");
    
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } 
  else {
    // if the file didn't open, print an error:
    Serial.println("error opening flie");
  }
  
}

void loop()
{
	// nothing happens after setup
}

