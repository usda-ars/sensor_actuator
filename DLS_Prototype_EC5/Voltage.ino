//-------------------------------------------------------------------------------
// getVolt() will add the input voltage read from A3 over a 10k / 15k voltage devider
// Dead Bug Protorypes
// 2014-02-07 -- created
// 2015-08-04 -- last updated
//-------------------------------------------------------------------------------

/*******************************************************************************
 * Name:        getVolt
 * Returns:     int, supply voltage (mV)
 * Parameters:  None.
 * Description: Returns supply voltage measured through a 10k/15k voltage 
 *              divider (R4 and R5 resistors); currently set to the analog 
 *              value because there's no way to back out actual input voltage.
 *              Vout = 0.6*Vin, where Vin = VREF*RAW/1023
 *              
 *              I'm not convinced that this does anything meaningful.
 ******************************************************************************/
int getVolt(){
  int vout_raw;    // 10-bit analog reading
  float temp;      // temporary variable
  int vin_mv;      // scaled supply voltage (mV)
  int Ra = 15000;    // R4 on DLS2.0
  int Rb = 10000;    // R5 on DLS2.0
  float sf = (1.0*Ra)/((float)(Ra + Rb));

  // Convert 10-bit analog to mV and scale to Vin
  vout_raw = analogRead(A3);
  return vout_raw;
  //temp = (float)vout_raw;
  //temp /= 1023.0;
  //temp *= VREF;
  //temp /= sf;
  //vin_mv = (int)(temp + 0.5);
  //return vin_mv;
  //strLogline += VoltReading;
  //strLogline += ';';
}
