/*******************************************************************************
 * DLS_Prototype_EC5.ino
 * 
 * written by Tyler W. Davis
 * USDA-Agricultural Research Service
 * Robert W. Holley Center for Agriculture & Health
 * Ithaca, NY 14853
 * 
 * 2015-07-10 -- created
 * 2015-10-08 -- last updated
 * 
 * ------------
 * description:
 * ------------
 * This script utilizes the DLS 2.0 shield (Dead Bug Prototypes) to measure
 * EC-5 volumetric soil moisture sensors (Decagon Devices) for a precision
 * agriculture application---controlling a solenoid valve for irrigation at
 * specified moisture levels.
 * 
 * ------
 * notes:
 * ------
 * 1. This code is based on the DLS_ShieldTest_10min code by Dead Bug 
 *    Prototypes created 2014-07-02
 *    
 * 2. This script requires third-party libraries, namely:
 *    RTClib.h : https://github.com/adafruit/RTClib
 * 
 * ----------
 * changelog:
 * ----------
 * 01. added SPI.h library (errors without it) [15.07.10]
 * 02. put #define statement values in ()'s [15.07.10]
 * 03. added datalog file initialization [15.07.10] 
 * 04. added NUMSAMPLES for VWC averaging [15.07.22]
 * 05. updated for Arduino Mega
 * 
 ******************************************************************************/

/* INCLUDE HEADERS */
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include "RTClib.h"


/**************************************************************
 * SENSOR CONFIGURATION 
 *   NUMSAMPLES ......... sensor measurements used for ave
 *   EXCITE_TIME ........ excitation time (milliseconds)
 *   TIME_BETWEEN_MEAS .. time between two consecutvie sensor 
 *                        measurements during the same sampling 
 *                        period (milliseconds)
 **************************************************************/
#define NUMSAMPLES (1)
#define EXCITE_TIME (10)
#define TIME_BETWEEN_MEAS (20)


/***************************************************
 * DATALOGGING CONFIGURATION
 *   SAMPLING_RATE_MIN .. sampling rate (minutes)
 *   DS1337_ADDRESS ..... real-time clock address
 *   SD_CHIPSELECT ...... SD card chip select
 *   VREF ............... reference for converting 
 *                        bits to voltage
 *   ISLOGGER ........... set to 0 for serial print
 **************************************************/
#define SAMPLING_RATE_MIN (10)
#define DS1337_ADDRESS (0x68)
#define SD_CHIPSELECT (8)
#define VREF (1100)
#define ISLOGGER (1)


/*****************************************************
 * INTERNAL STATE VARIABLES 
 *   RTC .............. DS1307 real-time clock object
 *   myFile ........... File object (for SD card)
 *   numberOfSensors .. number of analog sensors
 *   analogPins ....... array of analog sensor input 
 *                      pins (match with excitePins)
 *   excitePins ....... array of digital pins for 
 *                      excitation corresponding to 
 *                      their analog sensor inputs
 *   dataArray ........ integer array for storing 
 *                      sensor measurements and 
 *                      battery status; note that 
 *                      array size must have length 
 *                      one greater than expected 
 *                      number of entries!
 *   MyTimeStamp ...... char array for timestamping
 *****************************************************/
RTC_DS1307 RTC;
File myFile;
const int numberOfSensors = 4;
byte analogPins[] = {8, 9, 10, 11};
byte excitePins[] = {30, 28, 26, 24};
int dataArray[6];
char MyTimeStamp[] = "0000-00-00,00:00:00";


/***************************************************
 *  Name:        setup
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Make measurements and save to SD 
 *               card.
 *
 ***************************************************/
void setup()
{
  /*  Set analogReference to INTERVAL for Arduino 
   *  Uno (i.e., 1.1 Volts), or INTERNAL1V1 for 
   *  Arduino Mega, which allows finer resolution of 
   *  measurements (e.g., 320 in air to 980 in water)
   *  
   *  Prepare digital pins for excitation 
   *  (NPN transistor : LOW == OFF)
   */
  analogReference(INTERNAL1V1);  // for Arduino Mega
  for (byte i = 0; i < numberOfSensors; i++) {
    pinMode(excitePins[i], OUTPUT);
    digitalWrite(excitePins[i], LOW);
  }
  pinMode(10, OUTPUT);  // Are these necessary?
  pinMode(53, OUTPUT);  // 
  
  Serial.begin(9600);
  delay(1000);
  
  GetDate();
  for (byte i = 0; i < numberOfSensors; i++) {
    dataArray[i] = getEC5(analogPins[i], excitePins[i]);
    delay(TIME_BETWEEN_MEAS);
  }
  dataArray[numberOfSensors] = getVolt();
  WriteSD();
}

/***************************************************
 *  Name:        loop
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Sets wakeup alarm on RTC.
 *
 ***************************************************/
void loop()
{
  while (true)
  {
    SetAlarmMin(SAMPLING_RATE_MIN);
    delay(500);
  }
}

