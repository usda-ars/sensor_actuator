//-------------------------------------------------------------------------------
// WriteSD() writes data to the SD card.
// Using the SD library http://arduino.cc/en/Reference/SD
// Please note that chip select is set to pin 8
// Dead Bug Protorypes
// 
// 2014-02-07 -- created
// 2015-08-13 -- last updated
//
// changelog:
//   - renamed DATALOG.TXT to datalog.txt [15.07.10]
//   - added file existence checking, creates new if not [15.07.10]
//   - updated headerline & Serial print for numberOfSensors [15.08.13]
//-------------------------------------------------------------------------------

void WriteSD()
{
  if (!SD.begin(SD_CHIPSELECT)) {
    Serial.print("Initialization failed! "); 
    Serial.println("No SD card or not correctly mounted?");
    return;
  }

  // Check if datafile exists, if not, create it with headerline:
  if (!SD.exists("datalog.txt")) {
    Serial.println("Creating data log file.");
    myFile = SD.open("datalog.txt", FILE_WRITE);

    /* DATA FILE HEADER LINE */
    myFile.print("Timestamp,");
    for (byte i=0; i<numberOfSensors; i++) {
      myFile.print("EC5-");
      myFile.print(i);
      myFile.print("_bit,");
    }
    myFile.println("Battery_bit");
    myFile.close();
  }
  
  myFile = SD.open("datalog.txt", FILE_WRITE);
  if (myFile) {
    myFile.print(MyTimeStamp);
    myFile.print(",");
    for (byte i=0; i<numberOfSensors; i++) {
      myFile.print(dataArray[i]);
      myFile.print(",");
    }
    myFile.println(dataArray[numberOfSensors]);
    myFile.close();
    
    if (!ISLOGGER) {
      Serial.print("Writing: ");
      Serial.print(MyTimeStamp);
      Serial.print(",");
      for (byte i=0; i<numberOfSensors; i++) {
        Serial.print(dataArray[i]);
        Serial.print(",");
      }
      Serial.println(dataArray[numberOfSensors]);
    }
  } else {
    Serial.println("Error writing to datalog.txt");
  }
}

