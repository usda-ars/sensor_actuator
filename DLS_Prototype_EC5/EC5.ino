//-------------------------------------------------------------------------------
// EC5.ino
//
// written by Tyler W. Davis
//
// 2015-07-13 -- created
// 2015-08-13 -- last updated
//
// Measures EC5 from analog using Digital pin for excitation voltage
//
// changelog:
// 01. added if NUMSAMPLES > 1 for discharge time [15.08.13]
//-------------------------------------------------------------------------------

/***************************************************
 *  Name:        getEC5
 *
 *  Returns:     int, average sensor reading (10-bit)
 *
 *  Parameters:  - byte, analog pin reference (pin_a)
 *               - byte, excitation pin reference (pin_x)
 *
 *  Description: Returns 10-bit average value 
 *               corresponding to volumetric soil 
 *               moisture from an excitation voltage.
 *
 ***************************************************/
int getEC5(byte pin_a, byte pin_x){
  byte i;
  float ave;
  int samples[NUMSAMPLES+1];
  
  /*  Turn sensor on, 
   *  wait for excitation time,
   *  read the analog measurement,
   *  turn sensor off,
   *  allow sensor to discharge
   */ 
  for (i=0; i<NUMSAMPLES; i++) {
    digitalWrite(pin_x, HIGH);
    delay(EXCITE_TIME);
    samples[i] = analogRead(pin_a);
    digitalWrite(pin_x, LOW);
    if (NUMSAMPLES > 1) delay(4*EXCITE_TIME);
  }
  
  // Average all the samples
  ave = 0.0;
  for (i=0; i<NUMSAMPLES; i++) {
    ave += samples[i];
  }
  ave /= NUMSAMPLES;
  
  // Fast rounding w/ type casting:
  return (int)(ave + 0.5);
}

