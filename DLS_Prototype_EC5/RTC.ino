//-------------------------------------------------------------------------------
// RTC.ino
//
// written by Tyler W. Davis
// 
// 2015-07-10 -- created
// 2015-07-22 -- last updated
//
// Note: based on RTC.ino by Dead Bug Prototypes (2014-02-07)
// https://github.com/DeadBugPrototypes/DLS20
//
// GetDate() will add the current date and time to the char array MyTimeStamp
// SetAlarm() will reset the alarm (see DS1337 datasheet for dokumentation)
// 
//-------------------------------------------------------------------------------

void GetDate()
{
  Wire.begin(DS1337_ADDRESS);
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 0);	
  Wire.endTransmission();

  Wire.requestFrom(DS1337_ADDRESS, 7);
  uint8_t ss = bcd2bin(Wire.read());
  uint8_t mm = bcd2bin(Wire.read());
  uint8_t hh = bcd2bin(Wire.read());
  Wire.read();
  uint8_t d = bcd2bin(Wire.read());
  uint8_t m = bcd2bin(Wire.read());
  uint16_t y = bcd2bin(Wire.read()) + 2000;

  sprintf(
    MyTimeStamp,
    "%04d-%02d-%02d %02d:%02d:%02d", 
    y, m, d, hh, mm, ss 
  );
}

void SetAlarmMin(uint8_t AntallMin)
{
  Wire.begin(DS1337_ADDRESS);
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 0);	
  Wire.endTransmission();

  Wire.requestFrom(DS1337_ADDRESS, 2);
  Wire.read(); // hopper over sekunder
  uint8_t mm = bcd2bin(Wire.read());
  
  mm = mm + AntallMin;
  if (mm > 59) mm = mm - 60; 
  
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 7);
  Wire.write(0b00000000);      // Alarm 1 sekunder
  Wire.write(bin2bcd(mm));     // Alarm 1 minutter
  Wire.write(0b10000000);      // Alarm 1 timer (flagg satt)
  Wire.write(0b10000000);      // Alarm 1 dato (flagg satt)
  Wire.write(0b00000000);      // Alarm 2 minutter
  Wire.write(0b00000000);      // Alarm 2 timer (flagg satt)
  Wire.write(0b00000000);      // Alarm 2 dato (flagg satt)
  Wire.write(0b00011101);      // skru av ocillator og enable alarm 1
  Wire.write((byte) 0);        // Nullstill alt
  Wire.endTransmission();
}

void SetAlarm()
{
  Wire.beginTransmission(DS1337_ADDRESS);
  Wire.write((byte) 7);
  Wire.write(bin2bcd(29));     // Alarm 1 sekunder
  Wire.write(0b10000000);      // Alarm 1 minutter
  Wire.write(0b10000000);      // Alarm 1 timer (flagg satt)
  Wire.write(0b10000000);      // Alarm 1 dato (flagg satt)
  Wire.write(0b10000000);      // Alarm 2 minutter
  Wire.write(0b10000000);      // Alarm 2 timer (flagg satt)
  Wire.write(0b10000000);      // Alarm 2 dato (flagg satt)
  Wire.write(0b00011111);      // skru av ocillator og enable alarm 1 & 2
  Wire.write((byte) 0);        // Nullstill alt
  Wire.endTransmission();
}

// utility functions
static uint8_t bcd2bin (uint8_t val) { 
  return val - 6 * (val >> 4); 
}
static uint8_t bin2bcd (uint8_t val) { 
  return val + 6 * (val / 10); 
}

